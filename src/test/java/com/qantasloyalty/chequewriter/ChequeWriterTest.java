package com.qantasloyalty.chequewriter;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.Before;
import org.junit.Test;

public class ChequeWriterTest {

  private ChequeWriter chequeWriter;

  @Before
  public void setUp() {
    chequeWriter = new ChequeWriter();
  }

  @Test
  public void shouldInterpretSingleDigitNumber() {
    Map<Long, String> map = new HashMap<>();
    map.put(0L, "zero");
    map.put(1L, "one");
    map.put(2L, "two");
    map.put(3L, "three");
    map.put(4L, "four");
    map.put(5L, "five");
    map.put(6L, "six");
    map.put(7L, "seven");
    map.put(8L, "eight");
    map.put(9L, "nine");

    for (Entry<Long, String> integerStringEntry : map.entrySet()) {
      assertThat(chequeWriter.interpret(integerStringEntry.getKey()), is(integerStringEntry.getValue()));
    }
  }

  @Test
  public void shouldInterpret2DigitSingleWordNumbers() {
    Map<Long, String> map = new HashMap<>();
    map.put(10L, "ten");
    map.put(11L, "eleven");
    map.put(12L, "twelve");
    map.put(13L, "thirteen");
    map.put(14L, "fourteen");
    map.put(15L, "fifteen");
    map.put(16L, "sixteen");
    map.put(17L, "seventeen");
    map.put(18L, "eighteen");
    map.put(19L, "nineteen");
    map.put(20L, "twenty");
    map.put(30L, "thirty");
    map.put(40L, "forty");
    map.put(50L, "fifty");
    map.put(60L, "sixty");
    map.put(70L, "seventy");
    map.put(80L, "eighty");
    map.put(90L, "ninety");

    for (Entry<Long, String> integerStringEntry : map.entrySet()) {
      assertThat(chequeWriter.interpret(integerStringEntry.getKey()), is(integerStringEntry.getValue()));
    }
  }

  @Test
  public void shouldInterpret2DigitCompositeWordNumbers() {
    Map<Long, String> map = new HashMap<>();
    map.put(21L, "twenty one");
    map.put(32L, "thirty two");
    map.put(43L, "forty three");
    map.put(54L, "fifty four");
    map.put(65L, "sixty five");
    map.put(76L, "seventy six");
    map.put(87L, "eighty seven");
    map.put(98L, "ninety eight");

    for (Entry<Long, String> integerStringEntry : map.entrySet()) {
      assertThat(chequeWriter.interpret(integerStringEntry.getKey()), is(integerStringEntry.getValue()));
    }
  }

  @Test
  public void shouldInterpret3DigitNumbers() {
    Map<Long, String> map = new HashMap<>();
    map.put(100L, "one hundred");
    map.put(201L, "two hundred and one");
    map.put(318L, "three hundred and eighteen");
    map.put(432L, "four hundred and thirty two");

    for (Entry<Long, String> integerStringEntry : map.entrySet()) {
      assertThat(chequeWriter.interpret(integerStringEntry.getKey()), is(integerStringEntry.getValue()));
    }
  }

  @Test
  public void shouldInterpretLargeNumbers() {
    Map<Long, String> map = new HashMap<>();
    map.put(1_000L, "one thousand");
    map.put(20_000L, "twenty thousand");
    map.put(300_000L, "three hundred thousand");
    map.put(4_001L, "four thousand and one");
    map.put(50_020L, "fifty thousand and twenty");
    map.put(987_654L, "nine hundred and eighty seven thousand, six hundred and fifty four");
    map.put(123_987_654L, "one hundred and twenty three million, nine hundred and eighty seven thousand, six hundred and fifty four");
    map.put(69_123_987_654L, "sixty nine billion, one hundred and twenty three million, nine hundred and eighty seven thousand, six hundred and fifty four");
    map.put(18_000_000_054L, "eighteen billion and fifty four");
    map.put(976_518_000_000_054L, "nine hundred and seventy six trillion, five hundred and eighteen billion and fifty four");

    for (Entry<Long, String> integerStringEntry : map.entrySet()) {
      assertThat(chequeWriter.interpret(integerStringEntry.getKey()), is(integerStringEntry.getValue()));
    }
  }
}
