package com.qantasloyalty.chequewriter;

import java.util.HashMap;
import java.util.Map;

public class ChequeWriter {

  private final Map<Long, String> numberToWordMap = new HashMap<>();

  public ChequeWriter() {
    numberToWordMap.put(0L, "zero");
    numberToWordMap.put(1L, "one");
    numberToWordMap.put(2L, "two");
    numberToWordMap.put(3L, "three");
    numberToWordMap.put(4L, "four");
    numberToWordMap.put(5L, "five");
    numberToWordMap.put(6L, "six");
    numberToWordMap.put(7L, "seven");
    numberToWordMap.put(8L, "eight");
    numberToWordMap.put(9L, "nine");
    numberToWordMap.put(10L, "ten");
    numberToWordMap.put(11L, "eleven");
    numberToWordMap.put(12L, "twelve");
    numberToWordMap.put(13L, "thirteen");
    numberToWordMap.put(14L, "fourteen");
    numberToWordMap.put(15L, "fifteen");
    numberToWordMap.put(16L, "sixteen");
    numberToWordMap.put(17L, "seventeen");
    numberToWordMap.put(18L, "eighteen");
    numberToWordMap.put(19L, "nineteen");
    numberToWordMap.put(20L, "twenty");
    numberToWordMap.put(30L, "thirty");
    numberToWordMap.put(40L, "forty");
    numberToWordMap.put(50L, "fifty");
    numberToWordMap.put(60L, "sixty");
    numberToWordMap.put(70L, "seventy");
    numberToWordMap.put(80L, "eighty");
    numberToWordMap.put(90L, "ninety");
  }


  public String interpret(Long number) {
    if (number.equals(0L)) {
      return "zero";
    }

    StringBuilder word = new StringBuilder();
    interpretNumber(number, NumberPlace.TRILLION, word);
    return word.toString().trim();
  }

  private void interpretNumber(Long number, NumberPlace numberPlace, StringBuilder word) {
    if (number >= numberPlace.getValue()) {
      if (word.length() > 0) {
        if (numberPlace == NumberPlace.ONE && number < 100 && number > 0) {
          word.append(" and ");
        } else {
          word.append(", ");
        }
      }

      word.append(interpretThreeDigits(number / numberPlace.getValue())).append(' ').append(numberPlace.getName());
    }

    NumberPlace nextNumberPlace = NumberPlace.fromValue(numberPlace.getValue() / 1000);

    if (nextNumberPlace != null) {
      interpretNumber(number % numberPlace.getValue(), nextNumberPlace, word);
    }
  }

  private String interpretThreeDigits(Long number) {
    if (numberToWordMap.containsKey(number)) {
      return numberToWordMap.get(number);
    } else {
      StringBuilder word = new StringBuilder();
      if (number >= 100) {
        word.append(numberToWordMap.get(number / 100)).append(" hundred");
        if (number % 100 > 0) {
          word.append(" and ").append(interpretThreeDigits(number % 100));
        }
      } else {
        word.append(numberToWordMap.get(number / 10 * 10)).append(' ').append(numberToWordMap.get(number % 10));
      }

      return word.toString();
    }
  }
}

enum NumberPlace {
  TRILLION(1_000_000_000_000L, "trillion"),
  BILLION(1_000_000_000L, "billion"),
  MILLION(1_000_000L, "million"),
  THOUSAND(1_000L, "thousand"),
  ONE(1L, "");

  private final Long value;
  private final String name;

  NumberPlace(Long l, String name) {
    this.value = l;
    this.name = name;
  }

  public Long getValue() {
    return value;
  }

  public String getName() {
    return name;
  }

  public static NumberPlace fromValue(Long value) {
    for (NumberPlace numberPlace : NumberPlace.values()) {
      if (numberPlace.getValue().equals(value)) {
        return numberPlace;
      }
    }
    return null;
  }
}
